/**
 * The service module creates an ecs service, task definition
 * This module is intended to use with a target_group created
 * externally and passed as an argument.
 *
 * Usage:
 *
 *      module "auth_service" {
 *        source       = "git::ssh://git@bitbucket.org/ldfrtm/stack//service"
 *        name      = "auth-service"
 *        cluster   = "default"
 *      }
 *
 */

/**
 * Required Variables.
 */

variable "name" {
  description = "The service name, if empty the service name is defaulted to the image name"
  default     = ""
}

variable "environment" {
  description = "environment tag, e.g prod"
}

variable "cluster" {
  description = "The cluster name or ARN"
}

variable "container_port" {
  description = "The container port"
  default     = 80
}

variable "desired_count" {
  description = "The desired count"
  default     = 2
}

variable "iam_role" {
  description = "IAM Role ARN to use"
}

variable "policy" {
  description = "IAM custom policy to be attached to the task role"
  default     = ""
}

variable "container_definitions" {
  description = "here you should include the full container definitons"
}

variable "lb_target_container_name" {
  description = "Load balance Target Container"
}

variable "target-group-rule-priority" {
  description = "priority of the target group rule"
  default     = "100"
}

variable "hostname" {
  description = "Hostname of target group"
}

variable "health_check_grace_period_seconds" {
  description = "Amazon Elastic Container Service (Amazon ECS) service scheduler now allows you to define a grace period to prevent premature shutdown of newly instantiated tasks"
  default     = 0
}

variable "lb-arn" {}

variable "certificate-arn" {}

variable "vpc-id" {}

variable "deregistration_delay" {
  description = "Desregistration delay to container desregistre from ELB"
  default     = "30"
}

/* * Resources.
 */

// Task Role could be useful to grant special permissions 
// conveniently to the containers running into

resource "aws_alb_target_group" "black-hole" {
  name     = "${var.environment}-blackhole-lb-tg"
  port     = 80
  protocol = "HTTP"
  vpc_id   = "${var.vpc-id}"

  tags {
    environment = "${var.environment}"
  }
}

resource "aws_alb_listener" "front_end" {
  load_balancer_arn = "${var.lb-arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-TLS-1-2-2017-01"
  certificate_arn   = "${var.certificate-arn}"

  default_action {
    target_group_arn = "${aws_alb_target_group.black-hole.arn}"
    type             = "forward"
  }
}

resource "aws_alb_target_group" "main" {
  name                 = "${var.environment}-${var.name}-lb-tg"
  port                 = "${var.container_port}"
  protocol             = "HTTP"
  vpc_id               = "${var.vpc-id}"
  deregistration_delay = "${var.deregistration_delay}"

  tags {
    environment = "${var.environment}"
    hostname    = "${var.hostname}"
  }
}

resource "aws_lb_listener_rule" "main" {
  listener_arn = "${aws_alb_listener.front_end.arn}"
  priority     = "${var.target-group-rule-priority}"

  action {
    type             = "forward"
    target_group_arn = "${aws_alb_target_group.main.arn}"
  }

  condition {
    field  = "host-header"
    values = ["${var.hostname}"]
  }
}

resource "aws_iam_role" "main" {
  name = "${var.environment}-${var.name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "main" {
  count = "${var.policy == "" ? 0 : 1}"

  name   = "${var.environment}-${var.name}"
  role   = "${aws_iam_role.main.id}"
  policy = "${var.policy}"
}

resource "aws_ecs_service" "main" {
  name                               = "${var.name}"
  cluster                            = "${var.cluster}"
  task_definition                    = "${module.task.arn}"
  desired_count                      = "${var.desired_count}"
  iam_role                           = "${var.iam_role}"
  health_check_grace_period_seconds  = "${var.health_check_grace_period_seconds}"
  deployment_minimum_healthy_percent = "50"

  placement_strategy {
    type  = "spread"
    field = "attribute:ecs.availability-zone"
  }

  placement_constraints {
    type = "distinctInstance"
  }

  load_balancer {
    target_group_arn = "${aws_alb_target_group.main.arn}"
    container_name   = "${var.lb_target_container_name}"
    container_port   = "${var.container_port}"
  }
}

module "task" {
  source                = "git::https://github.com/egarbi/terraform-aws-task-definition?ref=1.0.0"
  name                  = "${var.environment}-${var.name}"
  task_role             = "${aws_iam_role.main.arn}"
  container_definitions = "${var.container_definitions}"
}

data "aws_lb" "lb" {
  arn = "${var.lb-arn}"
}

// The task role name used by the task definition
output "task_role" {
  value = "${aws_iam_role.main.name}"
}

// The task role arn used by the task definition
output "task_role_arn" {
  value = "${aws_iam_role.main.arn}"
}
