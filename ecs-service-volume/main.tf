/**
 * The service module creates an ecs service, task definition
 * This module is intended to use with a target_group created
 * externally and passed as an argument.
 *
 * Usage:
 *
 *      module "auth_service" {
 *        source       = "git::ssh://git@bitbucket.org/ldfrtm/stack//service"
 *        name      = "auth-service"
 *        cluster   = "default"
 *      }
 *
 */

/**
 * Required Variables.
 */

variable "name" {
  description = "The service name, if empty the service name is defaulted to the image name"
  default     = ""
}

variable "environment" {
  description = "environment tag, e.g prod"
}

variable "cluster" {
  description = "The cluster name or ARN"
}

variable "desired_count" {
  description = "The desired count"
  default     = 2
}

variable "iam_role" {
  description = "IAM Role ARN to use"
}

variable "policy" {
  description = "IAM custom policy to be attached to the task role"
  default     = ""
}

variable "container_definitions" {
  description = "here you should include the full container definitons"
}

variable "service_volume" {
  description = "container_path of volume mounted task definition"
}

variable "volume_host_path" {
  description = "host_path of volume mounted in instance"
}

/* * Resources.
 */

// Task Role could be useful to grant special permissions 
// conveniently to the containers running into

resource "aws_iam_role" "main" {
  name = "${var.environment}-${var.name}"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "main" {
  count = "${var.policy == "" ? 0 : 1}"

  name   = "${var.environment}-${var.name}"
  role   = "${aws_iam_role.main.id}"
  policy = "${var.policy}"
}

resource "aws_ecs_service" "main" {
  name                               = "${var.name}"
  cluster                            = "${var.cluster}"
  task_definition                    = "${module.task-volume.arn}"
  desired_count                      = "${var.desired_count}"
  deployment_minimum_healthy_percent = "50"

  placement_strategy {
    type  = "spread"
    field = "attribute:ecs.availability-zone"
  }

  placement_constraints {
    type = "distinctInstance"
  }
}

module "task-volume" {
  source                = "bitbucket.org/meupatrocinio/terraform-modules//task-volume"
  name                  = "${var.environment}-${var.name}"
  task_role             = "${aws_iam_role.main.arn}"
  container_definitions = "${var.container_definitions}"
  service_volume        = "${var.service_volume}"
  volume_host_path      = "${var.volume_host_path}"
}

// The task role name used by the task definition
output "task_role" {
  value = "${aws_iam_role.main.name}"
}

// The task role arn used by the task definition
output "task_role_arn" {
  value = "${aws_iam_role.main.arn}"
}
