/**
 * Variables.
 */

variable "name" {
  description = "Task definition name"
}

variable "container_definitions" {
  description = "here you should include the full container definitons"
  default     = "[]"
}

variable "task_role" {
  description = "Optional IAM role that tasks can use to make API requests to authorized AWS services."
  default     = ""
}

variable "service_volume" {
  description = "container_path of volume mounted task definition"
}

variable "volume_host_path" {
  description = "host_path of volume mounted in instance"
}

/**
 * Resources.
 */

# The ECS task definition.

resource "aws_ecs_task_definition" "main" {
  family        = "${var.name}"
  task_role_arn = "${var.task_role}"

  container_definitions = "${var.container_definitions}"

  volume {
    name      = "${var.service_volume}"
    host_path = "${var.volume_host_path}"
  }
}

/**
 * Outputs.
 */

// The created task definition name
output "name" {
  value = "${aws_ecs_task_definition.main.family}"
}

// The created task definition ARN
output "arn" {
  value = "${aws_ecs_task_definition.main.arn}"
}

// The created task definition ARN
output "revision" {
  value = "${aws_ecs_task_definition.main.revision}"
}
